package com.addapptr.nativeadssample;

import android.os.Bundle;

import com.intentsoftware.addapptr.AATKit;

import java.util.ArrayList;
import java.util.List;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class MainActivity extends AppCompatActivity {

    private static final int ITEMS_PER_AD = 10;
    private boolean firstAdRequested;
    private RecyclerViewAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        List<Object> itemsList = new ArrayList<>();

        RecyclerView recyclerView = findViewById(R.id.recyclerView);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);

        addSampleData(itemsList);
        addNativeAds(itemsList);

        int nativeAdPlacementId = ((SampleApplication) getApplication()).getNativeAdPlacementId();
        adapter = new RecyclerViewAdapter(itemsList, nativeAdPlacementId, this);
        recyclerView.setAdapter(adapter);
        ((SampleApplication) getApplication()).setNativeAdLoadListener(adapter);
    }

    @Override
    protected void onResume() {
        super.onResume();
        AATKit.onActivityResume(this);
        if (!firstAdRequested) { //prepare one ad at the very beginning
            firstAdRequested = true;
            int nativeAdPlacementId = ((SampleApplication) getApplication()).getNativeAdPlacementId();
            AATKit.reloadPlacement(nativeAdPlacementId);
        }
    }

    @Override
    protected void onPause() {
        AATKit.onActivityPause(this);
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        adapter.onDestroy();
        super.onDestroy();
    }

    private void addSampleData(List<Object> itemsList) {
        for (int i = 0; i < 50; i++) {
            itemsList.add(new ContentModel(i));
        }
    }

    private void addNativeAds(List<Object> itemsList) {
        int nativeAdPlacementId = ((SampleApplication) getApplication()).getNativeAdPlacementId();
        for (int i = ITEMS_PER_AD; i <= itemsList.size(); i += ITEMS_PER_AD) {
            itemsList.add(i, new AdModel(nativeAdPlacementId));
        }
    }
}