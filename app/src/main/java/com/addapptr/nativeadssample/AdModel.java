package com.addapptr.nativeadssample;

import com.intentsoftware.addapptr.AATKit;
import com.intentsoftware.addapptr.ad.NativeAdData;

public class AdModel {
    private NativeAdData nativeAd;
    private boolean reportedAdSpace;
    private final int nativeAdPlacementId;

    public AdModel(int nativeAdPlacementId) {
        this.nativeAdPlacementId = nativeAdPlacementId;
    }

    public void setNativeAd(NativeAdData nativeAd) {
        this.nativeAd = nativeAd;
    }

    public NativeAdData getNativeAd() {
        return nativeAd;
    }

    public void reportAdSpace() {
        if (!reportedAdSpace) { //report adspace only once for one ad
            reportedAdSpace = true;
            AATKit.reportAdSpaceForPlacement(nativeAdPlacementId);
        }
    }

    // Clear resources for no longer needed ads
    public void destroy() {
        if (nativeAd != null) {
            nativeAd.detachFromLayout();
        }
        nativeAd = null;
        reportedAdSpace = false;
    }
}
