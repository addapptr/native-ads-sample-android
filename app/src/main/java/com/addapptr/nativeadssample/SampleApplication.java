package com.addapptr.nativeadssample;

import android.app.Application;

import com.intentsoftware.addapptr.AATKit;
import com.intentsoftware.addapptr.AATKitConfiguration;
import com.intentsoftware.addapptr.AATKitReward;
import com.intentsoftware.addapptr.BannerPlacementLayout;
import com.intentsoftware.addapptr.ad.VASTAdData;

import androidx.annotation.Nullable;

public class SampleApplication extends Application implements AATKit.Delegate {

    interface NativeAdLoadListener {
        void onAdLoaded();
    }

    private int nativeAdPlacementId = -1;
    private NativeAdLoadListener nativeAdLoadListener;

    public void onCreate() {
        super.onCreate();
        AATKitConfiguration configuration = new AATKitConfiguration(this);

        configuration.setTestModeAccountId(74); //TODO: remember to remove it before publishing the app!
        configuration.setDelegate(this);

        AATKit.init(configuration);
        nativeAdPlacementId = AATKit.createNativeAdPlacement("Native", true);
    }

    public int getNativeAdPlacementId() {
        return nativeAdPlacementId;
    }

    public void setNativeAdLoadListener(NativeAdLoadListener nativeAdLoadListener) {
        this.nativeAdLoadListener = nativeAdLoadListener;
    }

    // AATKit.Delegate implementation:
    @Override
    public void aatkitHaveAd(int placementId) {
        if (nativeAdLoadListener != null && placementId == nativeAdPlacementId) {
            nativeAdLoadListener.onAdLoaded();
        }
    }

    @Override
    public void aatkitNoAd(int placementId) {
    }

    @Override
    public void aatkitPauseForAd(int placementId) {
    }

    @Override
    public void aatkitResumeAfterAd(int placementId) {
    }

    @Override
    public void aatkitShowingEmpty(int placementId) {
    }

    @Override
    public void aatkitUserEarnedIncentive(int placementId, @Nullable AATKitReward aatKitReward) {
    }

    @Override
    public void aatkitObtainedAdRules(boolean fromTheServer) {
    }

    @Override
    public void aatkitUnknownBundleId() {
    }

    @Override
    public void aatkitHaveAdForPlacementWithBannerView(int placementId, BannerPlacementLayout bannerView) {
    }

    @Override
    public void aatkitHaveVASTAd(int placementId, VASTAdData data) {
    }
}
