# Native ads sample for Addapptr

This app demonstrates native ads integration using AATKit. It presents how the native ads can be integrated within RecyclerView having multiple rows of content.
See also the [wiki](https://bitbucket.org/addapptr/native-ads-sample-android/wiki/) for more instrucions.

---
## Installation
Clone this repository and import into **Android Studio**
```bash
git clone git@bitbucket.org:addapptr/native-ads-sample-android.git
```
---
## Maintainers
Current maintainers:

* [Damian Supera](https://bitbucket.org/damian_s/)
* [Michał Kapuściński](https://bitbucket.org/m_kapuscinski/)
